require "poloniex/version"

require 'rest-client'
require 'openssl'
require 'addressable/uri'

module Poloniex
  class Client
    attr_accessor :configuration

    def initialize(key:, secret:)
      @key = key
      @secret = secret
    end

    def ticker
      get 'returnTicker'
    end

    def volume
      get 'return24hVolume'
    end

    def order_book( currency_pair )
      get 'returnOrderBook', currencyPair: currency_pair
    end

    def trade_history( currency_pair )
      get 'returnTradeHistory', currencyPair: currency_pair
    end

    def balances
      post 'returnBalances'
    end

    def open_orders( currency_pair )
      post 'returnOpenOrders', currencyPair: currency_pair
    end

    def trade_history( currency_pair )
      post 'returnTradeHistory', currencyPair: currency_pair
    end

    def buy( currency_pair, rate, amount )
      post 'buy', currencyPair: currency_pair, rate: rate, amount: amount
    end

    def sell( currency_pair, rate, amount )
      post 'sell', currencyPair: currency_pair, rate: rate, amount: amount
    end

    def cancel_order( currency_pair, order_number )
      post 'cancelOrder', currencyPair: currency_pair, orderNumber: order_number
    end

    def withdraw( curreny, amount, address )
      post 'widthdraw', currency: currency, amount: amount, address: address
    end

    private

    def resource
      @resouce ||= RestClient::Resource.new( 'https://www.poloniex.com' )
    end

    def get( command, params = {} )
      params[:command] = command
      resource[ 'public' ].get params: params
    end

    def post( command, params = {} )
      params[:command] = command
      params[:nonce]   = Time.now.to_i * 1000
      resource[ 'tradingApi' ].post params, { Key: @key , Sign: create_sign( params ) }
    end

    def create_sign( data )
      encoded_data = Addressable::URI.form_encode( data )
      OpenSSL::HMAC.hexdigest( 'sha512', @secret , encoded_data )
    end
  end
end
